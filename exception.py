import jwt
from jwt import InvalidTokenError, ExpiredSignatureError
from fastapi.responses import ORJSONResponse, HTMLResponse, RedirectResponse, PlainTextResponse, JSONResponse
from fastapi.exceptions import RequestValidationError

def validate_token_exc(app, validate_token):
    @app.exception_handler(InvalidTokenError)
    async def handle_error_invalid_token(request: validate_token, exc: InvalidTokenError):
        return JSONResponse({'response': 'Invalid access token'}, status_code=400)
    @app.exception_handler(ExpiredSignatureError)
    async def handle_error_expired_signature(request: validate_token, exc: ExpiredSignatureError):
        return JSONResponse({'response': 'Signature expired please login again'}, status_code=400)
    @app.exception_handler(Exception)
    async def handle_error_exception(request: validate_token, exc: Exception):
        return JSONResponse({'response': 'Access token error : ' + str(exc)}, status_code=400)

def register_data_exc(app, RegisterData):
    @app.exception_handler(RequestValidationError)
    async def handle_register_pydantic_error(request: RegisterData, exc: RequestValidationError):
        return JSONResponse({'response':'error : ' + f'{str(exc)}'}, status_code=400)