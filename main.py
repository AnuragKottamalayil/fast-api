from typing import ForwardRef, Optional
from unicodedata import name
from functools import wraps
from sqlalchemy.sql.functions import user
from fastapi import FastAPI, Query, Path, status, HTTPException, Depends, Response, Request
from fastapi.responses import ORJSONResponse, HTMLResponse, RedirectResponse, PlainTextResponse, JSONResponse
from fastapi.exceptions import RequestValidationError
from pydantic import BaseModel, Field, validator
from sqlalchemy import create_engine, and_, or_, not_, update, delete, desc, func, cast
import hashlib
import re
import jwt
from datetime import datetime, timedelta
from jwt import ExpiredSignatureError, InvalidTokenError
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session, relationship
from datetime import date
from email_validator import validate_email, EmailNotValidError
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer, HTTPBearer, HTTPBasic, HTTPBasicCredentials
from exception import validate_token_exc, register_data_exc
# from mangum import Mangum

app = FastAPI()



token_auth_scheme = HTTPBearer()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="fastapibasic")
# oauth2_scheme = OAuth2PasswordBearer(tokenUrl='UserLogin_2') error - canot add same url

# engine = create_engine('postgresql://postgres:postgres@localhost:5432/socialmedia')

# Development
Endpoint = 'athena.cexi2ietbeue.ap-south-1.rds.amazonaws.com'
Port = '5432'
MasterUsername = 'AthenaMaster'
MasterPassword = 'Ath3n4#M4st3R'

# develop db
engine = create_engine('postgresql://'+MasterUsername+':'+MasterPassword+'@'+Endpoint+':'+Port+'/socialmedia')

Base = automap_base()
Base.prepare(engine,reflect=True)
Session = sessionmaker()
session = Session.configure(bind=engine)

User = Base.classes.users   #Created tables
Post = Base.classes.post
Likes = Base.classes.likes
Date = Base.classes.date

s = Session()


class RegisterData(BaseModel):

    name : str = Field(None, max_length=100, min_length=2)
    mobile : str = Field(None, max_length=10, min_length=10)
    email : str
    username : str = Query(None, max_length=50, min_length=5)
    password : str

register_data_exc(app, RegisterData)
    
    # Custom validation error
    # @validator('username')
    # def username_validate(cls, v):
    #     if not v == 'james':
    #         raise ValueError('Invalid username')
    #     return v


class UserRegOut(BaseModel):
    name : str
    email : str

class LoginUser(BaseModel):
    username : str
    password : str


# User registration
@app.post("/UserRegistration", status_code=status.HTTP_201_CREATED)
async def post_method(response:Response, data : RegisterData):
    try:
        context = {}
        name = data.name
        email = data.email
        mobile = data.mobile
        username = data.username
        no_hash_password = data.password
        regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
            
        result = s.query(User).filter(User.username==username).first() # Checking for duplicate username in database
        
        if not result:                      # Checking password strength
            if not (len(no_hash_password) >= 8 \
                    and re.search("^[a-zA-Z0-9]+", no_hash_password) \
                    and re.search("[a-z]+", no_hash_password) \
                    and re.search("[A-Z]+", no_hash_password) \
                    and re.search("[0-9]+", no_hash_password)):
                    print('weakpassword')
                    context['response'] = 'Weakpassword'
                    response.status_code = 400
                    return context
            else:
                try:
                    valid = validate_email(email)
                    email = valid.email
                   
                    encoded_password = no_hash_password.encode()
                    passwrd = hashlib.sha256(encoded_password)
                    password = passwrd.hexdigest()
                    # print(password)
                    try:
                        add_user = User(name=name, username=username, ph_number=mobile, password=password, email=email)
                        s.add(add_user)
                        s.commit()
                        context["response"] = 'Account created successfully'
                        return context
                    except Exception as e:
                        print(e)
                        context["response"] = 'Error while creating ac'
                        response.status_code = 400
                        return context
                
                   
                except EmailNotValidError as e:
                    print(str(e))
                    context['response'] = 'Please enter a valid email'
                    response.status_code = 400
                    return context
                
        else:
            context['response'] = 'Username already exists'
            response.status_code = 400
            return context
        
    except Exception as e:
        print(e)
        response.status_code = 400
        return JSONResponse({'response':'error '+ f'{str(e)}'})


# User Authentication method 1------------------------------------------------------------------------------------>

@app.post("/UserLogin_1")
async def login(response:Response, form_data: OAuth2PasswordRequestForm = Depends()):
    try:
        context = {}
        username = form_data.username
        no_hash_password = form_data.password
        encoded_password = no_hash_password.encode()       # Generating a hashed password
        passwrd = hashlib.sha256(encoded_password)
        password = passwrd.hexdigest()    
        
        result = s.query(User).filter(and_(User.username==username, User.password==password)).first()
        print(result)
        if result:
            datas = {
                "username":username,
            }
            # Generating a jwt token if username and password are valid
            encoded_jwt_token = jwt.encode({"data":datas, "exp":datetime.utcnow() + timedelta(hours=24)}, "secret_key", algorithm="HS256")
            context['token'] = encoded_jwt_token
            context['response'] = 'Logged in'
            context['token type'] = "bearer"
            response.status_code = 200
            return context
        else:
            context['response'] = "Invalid username or password"
            response.status_code = 400
            return context
    except Exception as e:
        print(e)
        response.status_code = 400
        return JSONResponse({'error':f'{str(e)}'})


# User Authentication method 2------------------------------------------------------------------------------------>

@app.post("/fastapibasic")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    username = form_data.username
    no_hash_password = form_data.password
    encoded_password = no_hash_password.encode()       # Generating a hashed password
    passwrd = hashlib.sha256(encoded_password)
    password = passwrd.hexdigest() 
    result = s.query(User).filter(and_(User.username==username, User.password==password)).first()
    
    if not result:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return {"access_token": form_data.username + ' ' + form_data.password, "token_type": "bearer"}

@app.post("/UserLogin_2")
async def login(response:Response, credential : str = Depends(oauth2_scheme)):
    try:
        data = credential.split()
        
        context = {}
        username = data[0]
        
        datas = {
            "username":username,
        }
        encoded_jwt_token = jwt.encode({"data":datas, "exp":datetime.utcnow() + timedelta(hours=24)}, "secret_key", algorithm="HS256")
        context['token'] = encoded_jwt_token
        context['response'] = 'Logged in'
        context['token type'] = "bearer"
        response.status_code = 200
        return JSONResponse(context, status_code=200)
        
    except Exception as e:
        print(e)
        response.status_code = 400
        return JSONResponse({'error':f'{str(e)}'})


# User Authentication method 3 - HTTP basic Authentication ------------------------------------------------------------>

security = HTTPBasic()

@app.post('/http_login')
async def http_login(credentilas : HTTPBasicCredentials = Depends(security)):
    context = {}
    context['username'] = credentilas.username
    context['password'] = credentilas.password
    return JSONResponse(context, status_code = 200)


# ------------------------------------------------------------------------------------------------------------------------>

# validating jwt tokens method 1 ----------------------------------------------------------------------------------------->

async def validate_token(response:Response, token:str = Depends(token_auth_scheme)):
    token = token.credentials
    jwt_decode = jwt.decode(token, key='secret_key', algorithms=['HS256'])
    username = jwt_decode['data']['username']
    response.status_code = 200
    return username
 
@app.get("/private")
async def private_content(dependencies : str = Depends(validate_token)):
    validate_token_exc(app, validate_token)
    username = dependencies
    return {'username':username}

# ------------------------------------------------------------------------------------------------------------------------->

# Validating token method 2 ----------------------------------------------------------------------------------------------->

def validate_token_2(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        
        token = kwargs['token'].credentials
        try:
            jwt_decode = jwt.decode(token, key='secret_key', algorithms=['HS256'])
            username = jwt_decode['data']['username']
            return await func(username)
           
        except ExpiredSignatureError:
            # raise HTTPException(status_code=400, detail="Signature expired please login again")
            return JSONResponse({'response':'Signature expired please login again'}, status_code=400)
        except InvalidTokenError:
            # raise HTTPException(status_code=400, detail='Invalid access token')
            return JSONResponse({'response':'Invalid access token'}, status_code=400, )
    return wrapper

@app.get("/private_content_2")
@validate_token_2
async def private_content(username:Optional[str] = None, token:str = Depends(token_auth_scheme)):
    return {'data':'private datas', 'username':username}

# ---------------------------------------------------------------------------------------------------------------------->


@app.get("/items/")
async def read_items(q: Optional[str] = Query(None, min_length=3, max_length=50)):
    results = {"items": [{"item_id": "Foo"}, {"item_id": "Bar"}]}
    if q:
        results.update({"q": q})
    return results


# To return specific data
@app.post("/simplepost", response_model=UserRegOut)
async def post_method(data : RegisterData):
    return data


# changing response type and status codes
# To return response otherthan json
@app.get('/otherjsonresponse', response_class=ORJSONResponse)
def other_response():
    data = [{'data':'hai'}]
    return ORJSONResponse(content=data, status_code=200, media_type='text/plain')

# To return html response
@app.get('/htmlresponse')
def html_response():
    html_data = """
    <html>
        <head>
            <title>Some HTML in here</title>
        </head>
        <body>
            <h1>Look ma! HTML!</h1>
        </body>
    </html>
    """
    return HTMLResponse(content=html_data, status_code=200, media_type='text/html')

# redirectresponse
@app.get("/redirectresponse", response_class=RedirectResponse)
def redirect_resp():
    return "https://fastapi.tiangolo.com" 



# @app.get("/items/{item_id}")
# async def read_item(item_id: str, q: Optional[str] = None, short: bool = False):
#     item = {"item_id": item_id}
#     if q:
#         item.update({"q": q})
#     if not short:
#         item.update(
#             {"description": "This is an amazing item that has a long description"}
#         )
#     return item


# handler = Mangum(app=app)